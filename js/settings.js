


(function() {
	"use strict";
	
	// -------------------- 01. Preloader ---------------------
	// --------------------------------------------------------

	$(window).load(function() {
		$("#loader").fadeOut();
		$("#mask").delay(1000).fadeOut("slow");
	});
	

	
	// --------- 03. Adding fixed position to header ---------- 
	// --------------------------------------------------------
	
	$(document).scroll(function() {
		if ($(document).scrollTop() >= 1) {
		  $('.header-area').addClass('navbar-fixed-top');
		} else {
		  $('.header-area').removeClass('navbar-fixed-top');
		}
	});
	
	// -------------------- 04. Menu Toggle -------------------
	// --------------------------------------------------------
	
	$( ".togg-navi" ).click(function() {
		$( ".menu-main" ).toggle();
	});
	
	
	  // ------------------- 06. Scroll To Top ------------------
	// --------------------------------------------------------

	
	
	$(function() {
		$(document).on("scroll", onScroll);
		
		$('a[href*=#]:not([href=#])').click(function() {
			$('.menu-main li').removeClass('active');
			$(this).parent().addClass('active');
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
	
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});
	});
	function onScroll(event){
		var scrollPos = $(document).scrollTop();
		$('.menu-main li a[href*=#]:not([href=#])').each(function () {
			var currLink = $(this);
			var refElement = $(currLink.attr("href"));
			if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
				$('.menu-main li').removeClass("active");
				currLink.parent().addClass("active");
			}
			else{
				currLink.parent().removeClass("active");
			}
		});
	}
	  

})(jQuery);


